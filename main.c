////////////////////////////////////////////////////////////////////
// CHUONG TRINH KIEM TRA HOAT DONG ADC
// VIET BOI : PHAM VAN THUAN	DTVT07-K55-27/6/2013
// CHINH SUA: PHAM VAN THUAN	DTVT07-K55-27/6/2013
// Email:	terran991992@gmail.com
//		terran991992@yahoo.com
// Phone: 0166 380 8422
// .................................................................
// AP DUNG CHO MACH UNG DUNG MSP430
//******************************************************************/

//Khai bao cac thanh phan su dung
#define USE_CLOCK
#define USE_UART

////////////////////////////////////////////////////////////////////////////////
unsigned int Servo_PWM=1450,DC_PWM=1000;
char buff[50];

#include <wchar.h>
#include "stdio.h"
#include "string.h"
#include "msp430g2553.h"
#include "Clock.h"
#include "UART_thuan.h"


//Chu y Gyro bi sai so tinh khong dong nhat khi goc quay thay doi
// Khong tinh duoc vi phan cua gyro
void Set_PWM();

void main()
{
	WDTCTL = WDTPW | WDTHOLD;
	_delay_cycles(1000);
	//UART_Init();
	//UART_Write_Char(10);		//Ky tu xuong dong
	//UART_Write_String("Ket noi thanh cong");
	//UART_Write_Char(10);		//Ky tu xuong dong
	//_delay_cycles(100000);
	Set_PWM();
	//_delay_cycles(100000);
	//TAR=0;
	while(1)
	{
		Servo_PWM+=10;
		if(Servo_PWM>1600)Servo_PWM=1400;
		TA0CCR1=Servo_PWM;

		DC_PWM+=100;
		if(DC_PWM>1500)DC_PWM=500;
		TA1CCR1=DC_PWM;
		_delay_cycles(4000000);
	}
}

/*
// Timer A0 interrupt service routine
#pragma vector=TIMER0_A0_VECTOR
__interrupt void Timer_A0 (void)
{
	P2OUT^=BIT5;
	PWM1=2700;//Truc Y
	TA1CCR1 = PWM1;
	PWM2=2500;//Truc X	ok
	TA1CCR2 = PWM2;
}
*/
void Set_PWM()
{
	///////////////////////////////////////////////////////////////////////////////////////////
	Select_Clock(DCO_8MHZ);
	P1DIR|=BIT2;
	P1SEL|=BIT2;
	P1OUT&=~(BIT2);
	P2DIR|=BIT1+BIT2;         // P2.1 and P2.4 are outputs
	P2SEL |= BIT1;      // TA1.1 and TA1.2 are outputs for PWM on P2.1 and P2.4
	P2OUT&=~(BIT1);  //clear P2.1 and P2.4
	P2OUT|=BIT2;
	  // Start Timer_A
	TA1CCR0 = 2000;         // 2000Hz
	TA1CCR1 = 500;              // ~ 50% duty cycle on TA1.1 (P2.1)
	TA1CCTL1 = OUTMOD_7;   		// Reset/Set mode

	TA1CTL |= TASSEL_2 + MC_1 + ID_3; // Start timerA1 in up mode , SMCLK
	/////////////////////////////////////////////////////////////////////////////////////////////////
	TA0CCR0 = 20000 -1;
	TA0CCR1 = 1450;
	TA0CTL |= TASSEL_2 + MC_1 + ID_3; // Start timerA0 in up mode , SMCLK
	TA0CCTL1 = OUTMOD_7;   		// Reset/Set mode
}
/*

// Timer A0 interrupt service routine
#pragma vector=TIMER0_A0_VECTOR
__interrupt void Timer_A0 (void)
{

	if(P1OUT&BIT1)
	{
		TA0CCR0=1000;
	}
	else TA0CCR0=19000;
	P1OUT^=BIT1;
	//TAR=0;
	TA0CCTL0 = CCIE;
}
*/
